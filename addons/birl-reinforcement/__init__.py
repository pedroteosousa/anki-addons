import os
import random

from aqt import mw
from aqt.qt import *
from aqt.reviewer import Reviewer
from anki.hooks import wrap
from anki.sound import play
from aqt.utils import showInfo

from os import listdir
from os.path import isfile, join

# gets the 'birl' directory and all files inside
MODULE_NAME = __name__.split('.')[0]
ADDON_FOLDER = os.path.join(mw.pm.addonFolder(), MODULE_NAME)
AUDIO_FOLDER = os.path.join(ADDON_FOLDER, 'birl-reinforcement')

# get all audio files from the audio folder
ALL_FILES = [a for a in [f for f in listdir(AUDIO_FOLDER) if isfile(join(AUDIO_FOLDER, f))] if a[-4:] == ".mp3"]

# dif is the distance of the reinforcement
# spread is the maximum spread around dif
mw.birl = {
	"times": 0,
	"next": 0,
	"spread": 5,
	"dif": 20,
}

# plays a random mp3 file in the 'birl' folder
def birl():
	sound = ALL_FILES[random.randint(0, len(ALL_FILES)-1)]
	play(AUDIO_FOLDER + '/' + sound)

# gets time of the next reinforcement
def get_next():
	mw.birl["next"] = mw.birl["times"] + max(1, mw.birl["dif"] + random.randint(-mw.birl["spread"], mw.birl["spread"]));

# check if reinforcement is due
def maybe_birl(event):
  if mw.birl["times"] == mw.birl["next"]:
    birl()
    get_next()
  mw.birl["times"] += 1

# calls maybe_birl on every card shown
Reviewer.nextCard = wrap(Reviewer.nextCard, maybe_birl)
